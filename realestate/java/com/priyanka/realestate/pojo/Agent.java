package com.priyanka.realestate.pojo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.ManyToAny;




@Entity
@Table(name="agent")
@PrimaryKeyJoinColumn(name="personID")
public class Agent extends Person {
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="agent")
	private Set<PropertyFeature> property = new HashSet<PropertyFeature>();
	@ManyToMany(fetch= FetchType.EAGER)
	@JoinTable(name="AgentBuyerMap", joinColumns= @JoinColumn(name="AgentID"),
	inverseJoinColumns= @JoinColumn(name="BuyerID"))
	private Set<buyer> buyer = new HashSet<buyer>();
	
	
	public Agent(){
		
	}

	@Override
	public String toString(){
		return this.getUsername();
	}
	
	
	public Agent(String fname, String lname, String type, Date dob, String ssn, String username,String password, String email) {
		super(fname, lname, type, dob, ssn, username, password, email);
	}




	


	public Set<PropertyFeature> getProperty() {
		return property;
	}


	public void setProperty(Set<PropertyFeature> property) {
		this.property = property;
	}


	public Set<buyer> getBuyer() {
		return buyer;
	}


	public void setBuyer(Set<buyer> buyer) {
		this.buyer = buyer;
	}
	
	public void addprop(PropertyFeature properties){
		getProperty().add(properties);
	}

	public void addbuyer(buyer bu) {
		getBuyer().add(bu);
		
	}
		

}
