package com.priyanka.realestate.pojo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.priyanka.realestate.dao.PropertyfDAO;

@Entity
@Table(name="BuyerDetails")
@PrimaryKeyJoinColumn(name="buyerID")
public class buyer extends Person{
	
	@ManyToMany(mappedBy="buyer")
	private Set<Agent> agent= new HashSet<Agent>();
	@OneToMany(mappedBy="buyerr")
	private Set<PropertyFeature> property= new HashSet<PropertyFeature>();
	
	public buyer() {
		// TODO Auto-generated constructor stub
	}
	
	
	

	public buyer(String fname, String lname, String type, Date dob, String ssn, String username,
			String password, String email) {
		super(fname, lname, type, dob, ssn, username, password, email);
	}
	
	@Override
	public String toString(){
		return this.getUsername();
	}
	
	public Set<Agent> getAgent() {
		return agent;
	}
	public void setAgent(Set<Agent> agent) {
		this.agent = agent;
	}
	public Set<PropertyFeature> getProperty() {
		return property;
	}
	public void setProperty(Set<PropertyFeature> property) {
		this.property = property;
	}
	
	public void addprop(PropertyFeature properties){
		getProperty().add(properties);
	}




	public void addAgent(Agent ag) {
		getAgent().add(ag);
		
	}
	
	

}
