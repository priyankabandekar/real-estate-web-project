package com.priyanka.realestate.pojo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name="property")
@Inheritance(strategy= InheritanceType.JOINED)
public class Property{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="propertyID", unique= true, nullable=false)
	private long propertyID;
	@Column(name="rate")
	private long rate;
	
	@Column(name="location")
	private String location;//address
	@Column(name="hname")
	private String name;
	@Column(name="category")
	private String category;// rent, etc
	
public Property() {
	// TODO Auto-generated constructor stub
}
	
	public Property(long rate, String location, String name, String category) {
		super();
		this.rate = rate;
		this.location = location;
		this.name = name;
		this.category = category;
	}


	public long getPropertyID() {
		return propertyID;
	}

	public void setPropertyID(long propertyID) {
		this.propertyID = propertyID;
	}

	public long getRate() {
		return rate;
	}

	public void setRate(long rate) {
		this.rate = rate;
	}

	

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	


}
