package com.priyanka.realestate.pojo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;



@Entity
@Table(name="propertyFeature")
@PrimaryKeyJoinColumn(name="propertyId")
public class PropertyFeature extends Property{
	
	private String username;
	private String propfeature;
	@Column(name="descrp")
	@Lob
	private String descriptionofhouse;
	@Column(name="laundry")
	private boolean laundry;
	@Column(name="porch")
	private boolean porch;
	@Column(name="parking")
	private boolean parking;
	@Column(name="basement")
	private boolean basement;
	
	@Column(name="nofrooms")
	private int numofrooms;
	@Column(name="area")
	private float area;
	@Column(name="bath")
	private  int baths;
	@ManyToOne(fetch=FetchType.EAGER)
	@NotFound(action=NotFoundAction.IGNORE)
	@JoinColumn(name="agentID")
	private Agent agent;// agent who registered it
	@ManyToOne
	@NotFound(action=NotFoundAction.IGNORE)
	@JoinColumn(name="buyerID")
	private buyer buyerr;
	
	

	public PropertyFeature() {
	// TODO Auto-generated constructor stub

	}
	public PropertyFeature(String username,long rate, String location, String name, String category,
			String propfeature, String descriptionofhouse, boolean laundry, boolean porch, boolean parking,
			boolean basement, int numofrooms, float area, int baths, Agent agent) {
		super(rate, location, name, category);
		this.username = username;
		this.propfeature = propfeature;
		this.descriptionofhouse = descriptionofhouse;
		this.laundry = laundry;
		this.porch = porch;
		this.parking = parking;
		this.basement = basement;
		this.numofrooms = numofrooms;
		this.area = area;
		this.baths = baths;
		this.agent = agent;
		
	}


	public PropertyFeature(String username,long rate, String location, String name, String category, String propfeature,
			String descriptionofhouse, boolean laundry, boolean porch, boolean parking, boolean basement,
			int numofrooms, float area, int baths) {
		super(rate, location, name, category);
		this.username=username;
		this.propfeature = propfeature;
		this.descriptionofhouse = descriptionofhouse;
		this.laundry = laundry;
		this.porch = porch;
		this.parking = parking;
		this.basement = basement;
		this.numofrooms = numofrooms;
		this.area = area;
		this.baths = baths;
	}
	
	
	public Agent getAgent() {
		return agent;
	}


	public void setAgent(Agent agent) {
		this.agent = agent;
	}


	public buyer getBuyerr() {
		return buyerr;
	}


	public void setBuyerr(buyer buyerr) {
		this.buyerr = buyerr;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPropfeature() {
		return propfeature;
	}
	public void setPropfeature(String propfeature) {
		this.propfeature = propfeature;
	}
	public String getDescriptionofhouse() {
		return descriptionofhouse;
	}
	public void setDescriptionofhouse(String descriptionofhouse) {
		this.descriptionofhouse = descriptionofhouse;
	}
	public boolean isLaundry() {
		return laundry;
	}
	public void setLaundry(boolean laundry) {
		this.laundry = laundry;
	}
	public boolean isPorch() {
		return porch;
	}
	public void setPorch(boolean porch) {
		this.porch = porch;
	}
	public boolean isParking() {
		return parking;
	}
	public void setParking(boolean parking) {
		this.parking = parking;
	}
	public boolean isBasement() {
		return basement;
	}
	public void setBasement(boolean basement) {
		this.basement = basement;
	}
	public int getNumofrooms() {
		return numofrooms;
	}
	public void setNumofrooms(int numofrooms) {
		this.numofrooms = numofrooms;
	}
	public float getArea() {
		return area;
	}
	public void setArea(float area) {
		this.area = area;
	}
	public int getBaths() {
		return baths;
	}
	public void setBaths(int baths) {
		this.baths = baths;
	}
	
	
	
	

}
