package com.priyanka.realestate.pojo;

import javax.persistence.Embeddable;

@Embeddable
public class Address {
	
	private String street;
	private String city;
	private String state;
	private long pincode;
	
	public Address(){
		
	}
	
	
	public Address(String street, String city, String state, long pincode) {
		
		this.street = street;
		this.city = city;
		this.state = state;
		this.pincode = pincode;
	}


	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public long getPincode() {
		return pincode;
	}
	public void setPincode(long pincode) {
		this.pincode = pincode;
	}
	

	
}
