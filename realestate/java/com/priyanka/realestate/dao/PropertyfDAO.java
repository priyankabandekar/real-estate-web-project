package com.priyanka.realestate.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.validator.internal.engine.messageinterpolation.parser.BeginState;

import com.priyanka.realestate.exception.AdException;
import com.priyanka.realestate.pojo.Agent;
import com.priyanka.realestate.pojo.Person;
import com.priyanka.realestate.pojo.Property;
import com.priyanka.realestate.pojo.PropertyFeature;

public class PropertyfDAO extends DAO {
	

	public PropertyfDAO() {
		// TODO Auto-generated constructor stub
	}
	
	public PropertyFeature create(String username,long rate, String location, String name, String category, String propfeature,
			String descriptionofhouse, boolean laundry, boolean porch, boolean parking, boolean basement,
			int numofrooms, float area, int baths, Agent agent) throws AdException
	{
		try{
			
		begin();
		
		PropertyFeature property = new PropertyFeature(username,rate, location, name, category, propfeature, descriptionofhouse, laundry, porch, parking, basement, numofrooms, area, baths,agent);
		
		getSession().save(property);
		
		commit();
		return property;
		
		}catch(HibernateException e){
			rollback();
			throw new AdException("Exception while creating agent: " + e.getMessage());
			
		}
	}
	
	public List getallprop(){
		List prop = new ArrayList();
		
		prop=getSession().createCriteria(PropertyFeature.class).list();
		return prop;
		
	}
	

	public List getonly(String username){
		List prop = new ArrayList();
		
		Criteria criteria= getSession().createCriteria(PropertyFeature.class);
		criteria.add(Restrictions.eq("username", username));
		prop= criteria.list();
		return prop;
				
	}
	
	public List getbylocation(String location){
		List loca = new ArrayList();
		Criteria criteria= getSession().createCriteria(PropertyFeature.class);
		criteria.add(Restrictions.eq("location", location));
		loca= criteria.list();
		return loca;
	}
	
	public PropertyFeature get( String name){
	Criteria crit = getSession().createCriteria(PropertyFeature.class);
	crit.add(Restrictions.eq("name", name));
	PropertyFeature f = (PropertyFeature) crit.uniqueResult();
	
		return f;
	}
	 public void delete(String name)
	            throws AdException {
	        try {
	            begin();
	            getSession().delete(get(name));
	            commit();
	        } catch (HibernateException e) {
	            rollback();
	            throw new AdException("Could not delete advert", e);
	        }
	    }
	 
	 public void edit(PropertyFeature pf){
		 getSession().update(pf);
	 }

	public boolean checkifpropertyexixts(String name) {
		
		boolean propfound= false;
		
		Criteria c= getSession().createCriteria(PropertyFeature.class);
		c.add(Restrictions.eq("name", name));
		List list = c.list();
		
		if((list!=null)&& (list.size()>0)){
			propfound=true;
		}
		return propfound;
		
	}

	public boolean checkiflocationexists(String location) {
		boolean locationfound=false;
		Criteria c= getSession().createCriteria(PropertyFeature.class);
		c.add(Restrictions.eq("location", location));
		List list = c.list();
		
		if((list!=null)&& (list.size()>0)){
			locationfound=true;
		}
		return locationfound;
	}

	public PropertyFeature update(String username, long rate, String location, String name, String category,
			String propfeature, String desc, boolean laundry, boolean porch, boolean parking, boolean basement,
			int numofrooms, float area, int baths, Agent agent) throws AdException {
		
		try{
			
			begin();
			
			PropertyFeature property = new PropertyFeature(username,rate, location, name, category, propfeature, desc, laundry, porch, parking, basement, numofrooms, area, baths,agent);
			System.out.println("after");
			getSession().update(property);
			getSession().flush();
			commit();
			return property;
			
			}catch(HibernateException e){
				rollback();
				throw new AdException("Exception while creating agent: " + e.getMessage());
				
			}
		
	}

	public void Updateprop(PropertyFeature userprop) {
		getSession().merge(userprop);
		
	}

	
	
	
	
}
