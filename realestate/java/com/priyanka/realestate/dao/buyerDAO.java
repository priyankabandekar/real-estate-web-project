package com.priyanka.realestate.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import com.priyanka.realestate.exception.AdException;
import com.priyanka.realestate.pojo.Address;
import com.priyanka.realestate.pojo.Agent;
import com.priyanka.realestate.pojo.buyer;

public class buyerDAO extends DAO {
	public buyerDAO() {
		// TODO Auto-generated constructor stub
	}

	public buyer create(String username, String password, String email, String fname, String lname, String type,Date dob, String ssn,String street, String city, String state, long pincode) throws AdException {
		
		try{
			begin();
			buyer buyer= new buyer(fname, lname, type, dob, ssn, username, password, email);
			Address address= new Address(street, city, state, pincode);
			buyer.setAddress(address);
			getSession().save(buyer);
			commit();
			return buyer;
		}
		catch(HibernateException e){
			rollback();
			throw new AdException("Exception while creating buyer: " + e.getMessage());
		}
		
	}
	
	public buyer get(String username) throws AdException{
		try{
			begin();
			Criteria q = getSession().createCriteria(buyer.class);
			q.add(Restrictions.eq("username", username));
			buyer buyer = (buyer) q.uniqueResult();
			commit();
			return buyer;
			
			
		} catch (HibernateException e) {
            rollback();
            throw new AdException("Could not obtain the named buyer " + e.getMessage());
        }
		
	}
	
	
	public void save(buyer buyer)throws AdException {
		// TODO Auto-generated method stub
		try{
			begin();
			System.out.println("hi");
			getSession().update(buyer);
			commit();
			
		} catch (HibernateException e) {
            rollback();
            throw new AdException("Could not save the buyer", e);
        }
		
	}
	
	public void delete(buyer buyer) throws AdException {
        try {
            begin();
            getSession().delete(buyer);
            commit();
        } catch (HibernateException e) {
            rollback();
            throw new AdException("Could not delete the buyer", e);
        }
    }

	public boolean checkifbuyer(String username) {
		boolean buyerfound= false;
		
		Criteria c = getSession().createCriteria(buyer.class);
		c.add(Restrictions.eq("username", username));
		List list = c.list();
		if((list!=null)&& (list.size()>0)){
			buyerfound = true;
		}
		return buyerfound;
	}
	
	
	

}
