package com.priyanka.realestate.dao;




import java.util.Date;
import java.util.List;

import javax.security.auth.login.CredentialException;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import com.priyanka.realestate.exception.AdException;
import com.priyanka.realestate.pojo.*;


public class AgentDAO extends DAO {

	public AgentDAO() {
	// TODO Auto-generated constructor stub

	}
	
	public Agent create(String username, String password, String email, String fname, String lname, String type,Date dob, String ssn,String street, String city, String state, long pincode) throws AdException{
		try{
		begin();
		System.out.print("test3");
		Agent agent = new Agent(fname, lname, type, dob, ssn, username, password, email);
		Address address= new Address(street, city, state, pincode);
		agent.setAddress(address);
		getSession().save(agent);
		commit();
		return agent;
		}
		catch(HibernateException e){
			rollback();
			throw new AdException("Exception while creating agent: " + e.getMessage());
			
		}
		
	}
	
	public Agent get(String username) throws AdException{
		try{
			begin();
			Criteria q = getSession().createCriteria(Agent.class);
			q.add(Restrictions.eq("username", username));
			Agent agent = (Agent) q.uniqueResult();
			commit();
			return agent;
			
			
		} catch (HibernateException e) {
            rollback();
            throw new AdException("Could not obtain the named Agent " + e.getMessage());
        }
		
	}
	
	public boolean checkifagent(String username){
		boolean agentfound= false;
		
		Criteria c = getSession().createCriteria(Agent.class);
		c.add(Restrictions.eq("username", username));
		List list = c.list();
		if((list!=null)&& (list.size()>0)){
			agentfound = true;
		}
		return agentfound;
	}

	public void save(Agent agent)throws AdException {
		// TODO Auto-generated method stub
		try{
			begin();
			getSession().update(agent);
			commit();
			
		} catch (HibernateException e) {
            rollback();
            throw new AdException("Could not save the agent", e);
        }
		
	}
	
	public void delete(Agent agent) throws AdException {
        try {
            begin();
            getSession().delete(agent);
            commit();
        } catch (HibernateException e) {
            rollback();
            throw new AdException("Could not delete the agent", e);
        }
    }

	public List list() throws AdException {
		try{
			begin();
			Criteria q = getSession().createCriteria(Agent.class);
			List results = q.list();
			commit();
			return results;
		}catch (HibernateException e) {
            rollback();
            throw new AdException("Could not list the properties", e);
        }
	}
	
}
