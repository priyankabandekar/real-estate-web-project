	package com.priyanka.realestate.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import com.priyanka.realestate.exception.AdException;
import com.priyanka.realestate.pojo.Address;
import com.priyanka.realestate.pojo.Agent;
import com.priyanka.realestate.pojo.Person;
import com.priyanka.realestate.pojo.buyer;

public class UserDAO extends DAO {
	public UserDAO() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public Person get(String username) throws AdException
	{
		try{
			begin();
			Query q = getSession().createQuery("from Person where username= :username");
			q.setString("username", username);
			Person user=(Person) q.uniqueResult();
			
			
			commit();
			return user;
			
		}catch(HibernateException e){
			rollback();
			throw new AdException("Could not get user " + username, e);
					
		}
		
	}
	
	public Person create(String username, String password, String email, String fname, String lname, String type,Date dob, String ssn,String street, String city, String state, long pincode) throws AdException
	
	{
		try{
			 System.out.println("inside DAO");
		
			begin();
			
			Person person = new Person(fname, lname, type, dob, ssn, username, password, email);
			Address address= new Address(street, city, state, pincode);
			
			person.setAddress(address);
			
			getSession().save(person);
			
			commit();
			
			return person;
			
		}catch(HibernateException e){
			rollback();
			
			throw new AdException("Exception while creating user: " + e.getMessage());
			
		}
		
		
	}
	
	public boolean checkifusername(String username){
		
		boolean userfound= false;
		Criteria c = getSession().createCriteria(Person.class);
		c.add(Restrictions.eq("username", username));
		List list = c.list();
		
		if((list!=null) && (list.size()>0))
		{
			userfound= true;
		}
		return userfound;
		
	}
	
	public boolean checklogin(String username, String password, String type) {
		boolean userfound= false;
		
		String q= "from Person where username=? and password=? and type=?";
		Query query = getSession().createQuery(q);
		query.setParameter(0, username);
		query.setParameter(1, password);
		query.setParameter(2, type);
		
		List list = query.list();
		
		if((list != null) && (list.size()>0)){
			userfound= true;
			
		}
		
		return userfound;
		
	}
	
	
	public void delete(Person user) throws AdException{
		try{
		
		begin();
		getSession().delete(user);
		commit();
		}
		catch(HibernateException e){
			rollback();
			throw new AdException("Could not delete user: " + e.getMessage());
		}
	}
	

}
