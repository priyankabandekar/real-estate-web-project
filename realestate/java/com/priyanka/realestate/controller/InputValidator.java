package com.priyanka.realestate.controller;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.priyanka.realestate.pojo.Person;


public class InputValidator implements Validator {
	
	public boolean supports(Class aClass)
	{
		return aClass.equals(Person.class);
	}
	
	public void validate(Object obj, Errors errors){
		
		Person user = (Person) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fname", "error.invalid.user", "Invalid Details- First Name Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lname", "error.invalid.user", "Invalid Details- Last Name Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.street", "error.invalid.user.address", "Invalid Details- Street Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.city", "error.invalid.user.address", "Invalid Details- City Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.state", "error.invalid.user.address", "Invalid Details- State Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.pincode", "error.invalid.user.address", "Invalid Details- Pincode Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dob", "error.invalid.user", "Invalid Details- Date of Birth required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ssn", "error.invalid.user", "Invalid Details- SSN required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "error.invalid.user", "Invalid Details- Username required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "error.invalid.user", "Invalid Details");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "error.invalid.user", "Invalid Details- Email required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type", "error.invalid.user", "Invalid Details- Type required");


		
	}

	

}
