package com.priyanka.realestate.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.priyanka.realestate.dao.UserDAO;
import com.priyanka.realestate.pojo.Person;
import com.priyanka.realestate.pojo.PropertyFeature;


@Controller
@RequestMapping("/login.htm")
public class LoginController {
	
	@ModelAttribute("userprop")
	public PropertyFeature construct(){
		return new PropertyFeature();
	}
	 
	
	
	@RequestMapping(method= RequestMethod.POST)
	public String loginFormAction(@ModelAttribute("user") Person user, BindingResult results, Model model){
		
		
		
		if(results.hasErrors()){
			return "login";
			
		}
		
		UserDAO userdao = new UserDAO();
		
		boolean userexists = userdao.checklogin(user.getUsername(), user.getPassword(), user.getType());
		if(userexists){
			model.addAttribute(user);
			
			if(user.getType().equalsIgnoreCase("agent")){
				return "agent";
			}
			else if(user.getType().equalsIgnoreCase("buyer"))
			{
				return "buyer";
			}
			else
			{
				return "login";
			}
			
		}
		else{
			results.rejectValue("username", "invalidusername");
			return "login";
			
		}
	}
	
	@RequestMapping(method= RequestMethod.GET)
	public String initializeForm(@ModelAttribute("user") Person user, BindingResult result){
		return "login";
	}

	
	

}
