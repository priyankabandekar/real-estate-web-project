package com.priyanka.realestate.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.priyanka.realestate.dao.PropertyfDAO;
import com.priyanka.realestate.pojo.PropertyFeature;

@Controller
@RequestMapping("/buyer.htm")
public class BuyerController {
	
	
	@Autowired
	@Qualifier("locValidator")
	LocationValidator validator;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@ModelAttribute("buyy")
	public PropertyFeature construct(){
		return new PropertyFeature();
	}
	
	@RequestMapping(method= RequestMethod.GET)
	public String initializeForm(@ModelAttribute("userprop") PropertyFeature userprop, BindingResult result){
		return "buyer";
	}
	
	@RequestMapping(params="listall",method=RequestMethod.POST)
	public ModelAndView seeallproducts(@ModelAttribute("userprop")PropertyFeature userprop, BindingResult result){
		PropertyfDAO dao = new PropertyfDAO();
		List a = new ArrayList();
		 a = dao.getallprop();
		 List b = new ArrayList();
		 
		 Iterator propit = a.iterator();
			while(propit.hasNext()){
				 PropertyFeature prop = (PropertyFeature) propit.next();
				 b.add(prop);
			}
		 
		return new ModelAndView("viewlist","properties", b);
		
		
	}
	
	@RequestMapping(params="locat", method= RequestMethod.POST)
	public ModelAndView listbylocation(@ModelAttribute("userprop")PropertyFeature userprop, BindingResult result){
		
		validator.validate(userprop, result);
		String location= userprop.getLocation();
		PropertyfDAO  dao= new PropertyfDAO();
		List a = new ArrayList();
		
		if(dao.checkiflocationexists(location))
		{
		a= dao.getbylocation(location);
		List b = new ArrayList();
		 
		 Iterator propit = a.iterator();
			while(propit.hasNext()){
				 PropertyFeature prop = (PropertyFeature) propit.next();
				 b.add(prop);
			}
		 
		return new ModelAndView("viewlist","properties", b);
		}
		else{
			result.rejectValue("location", "locationnotfound");
			return new ModelAndView("buyer");
		}
	}
	
	@RequestMapping(params="logout", method=RequestMethod.POST)
	public String logout(@ModelAttribute("userprop") PropertyFeature userprop, BindingResult result){
		return "logout";
		
	}
	
	@RequestMapping(params="buyerpg", method=RequestMethod.POST)
	public String buyeradd(@ModelAttribute("userprop") PropertyFeature userprop, BindingResult result){
		return "buyer";
		
	}
	

}
