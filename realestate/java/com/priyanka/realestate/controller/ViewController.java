package com.priyanka.realestate.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.priyanka.realestate.pojo.PropertyFeature;
@Controller
@RequestMapping("/viewlist.htm")
public class ViewController {
	
	
	@RequestMapping(params="bot",method= RequestMethod.POST)
	public String onSubmit(@ModelAttribute("buyy") PropertyFeature buy, BindingResult result){
		if(result.hasErrors()){
			return "viewlist";
		}

		
		return "buy";
		
	}
	
	@RequestMapping(params="logout", method=RequestMethod.POST)
	public String logout(@ModelAttribute("userprop") PropertyFeature userprop, BindingResult result){
		return "logout";
		
	}
	
}