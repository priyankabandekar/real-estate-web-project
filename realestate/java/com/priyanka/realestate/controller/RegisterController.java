package com.priyanka.realestate.controller;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.*;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.priyanka.realestate.dao.AgentDAO;
import com.priyanka.realestate.dao.UserDAO;
import com.priyanka.realestate.dao.buyerDAO;
import com.priyanka.realestate.pojo.Agent;
import com.priyanka.realestate.pojo.Person;
import com.priyanka.realestate.exception.AdException;

@Controller
@RequestMapping("/register.htm")
public class RegisterController {
	@Autowired
	@Qualifier("fieldValidator")
	InputValidator validator;
	
	@InitBinder
	private void initBinder(WebDataBinder binder){
		 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
		binder.setValidator(validator);
	}
	
	@RequestMapping(method= RequestMethod.POST)
	protected String doSubmitAction(@ModelAttribute("user") Person user, BindingResult result) throws Exception{
		
		validator.validate(user, result);
		if(result.hasErrors()){
			return "register";
		}
			System.out.print("test");
			UserDAO userDao = new UserDAO();
			AgentDAO agentdao = new AgentDAO();
			buyerDAO buyerDao = new buyerDAO();
			
			System.out.print("test1");
			String username= user.getUsername();
			boolean userexists = userDao.checkifusername(username);
			
			if(!userexists){
			
			if(user.getType().equalsIgnoreCase("agent") || user.getType().equalsIgnoreCase("buyer"))
			{
			if(user.getType().equalsIgnoreCase("agent")){
				System.out.print("test1");
			      agentdao.create(user.getUsername(), user.getPassword(), user.getEmail(), user.getFname(), user.getLname(), user.getType(), user.getDob(), user.getSsn(), user.getAddress().getStreet(),
							user.getAddress().getCity(), user.getAddress().getState(), user.getAddress().getPincode());
				}
			else if(user.getType().equalsIgnoreCase("buyer")){
					
					buyerDao.create(user.getUsername(), user.getPassword(), user.getEmail(), user.getFname(), user.getLname(), user.getType(), user.getDob(), user.getSsn(), user.getAddress().getStreet(),
							user.getAddress().getCity(), user.getAddress().getState(), user.getAddress().getPincode());
					
				}
			
			return "success";
			}
			else{
				result.rejectValue("type", "invaliduserdetails");
				return "redirect/:register.htm";
			}
			}
			else{
				result.rejectValue("username", "invaliduserexists");
				return "register";
			}
			
		
	}
	

	@RequestMapping(method= RequestMethod.GET)
	public String initializeForm(@ModelAttribute("user") Person user, BindingResult result){
		return "register";
	}
	

}
