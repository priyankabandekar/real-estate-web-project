package com.priyanka.realestate.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import com.priyanka.realestate.dao.AgentDAO;
import com.priyanka.realestate.dao.DAO;
import com.priyanka.realestate.dao.PropertyfDAO;
import com.priyanka.realestate.exception.AdException;
import com.priyanka.realestate.pojo.Agent;
import com.priyanka.realestate.pojo.Person;
import com.priyanka.realestate.pojo.PropertyFeature;

@Controller
public class AgentController {
	@Autowired
	@Qualifier("propValidator")
	PropertyValidator validator;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@RequestMapping(value="/agent.htm",params="addprop", method=RequestMethod.POST)
	public ModelAndView addproperty(@ModelAttribute("userprop") PropertyFeature userprop,BindingResult result, Model model )
	{
		validator.validate(userprop, result);
		if(result.hasErrors()){
			//return "agent";
		
			return new ModelAndView("agent");
		}
		String username= userprop.getUsername();
		long rate = userprop.getRate();
		String location= userprop.getLocation();
		String name= userprop.getName();
		String category = userprop.getCategory();
		String propfeature= userprop.getPropfeature();
		String desc= userprop.getDescriptionofhouse();
		boolean laundry = userprop.isLaundry();
		boolean porch= userprop.isPorch();
		boolean parking = userprop.isParking();
		boolean basement = userprop.isBasement();
		int numofrooms= userprop.getNumofrooms();
		float area= userprop.getArea();
		int baths = userprop.getBaths();
		AgentDAO agentdao = new AgentDAO();
		PropertyfDAO property = new PropertyfDAO();
		
		boolean propnameexists= property.checkifpropertyexixts(name);
		
		if(!propnameexists){
		try{
			
			
			Agent agent = agentdao.get(username);
			
			PropertyFeature prop = property.create(username, rate, location, name, category, propfeature, desc, laundry, porch, parking, basement, numofrooms, area, baths,agent);		
		
			agent.addprop(prop);//add prop to agent
			agentdao.save(agent);//update agent
			
			model.addAttribute(userprop);
		} catch (AdException e) {
            System.out.println(e.getMessage());
        }
		
		return new ModelAndView("redirect:/agent.htm", "prop", userprop);	
		}
		else{
			result.rejectValue("name", "nameexists");
			return new ModelAndView("agent");
		}
		
	}
	
	@RequestMapping(value="/agent.htm", params="nav", method=RequestMethod.POST)
	public String navigation(@ModelAttribute("userprop") PropertyFeature userprop,BindingResult result){
		return "agentnav";
	}
	

	@RequestMapping(value="/agent.htm",params="next", method=RequestMethod.POST)
	public ModelAndView next(@ModelAttribute("userprop") PropertyFeature userprop,BindingResult result, Model model ){
		System.out.println("heya");
		
		
		String username= userprop.getUsername();
		
		
			PropertyfDAO dao = new PropertyfDAO();
		AgentDAO agdao = new AgentDAO();
		 List b = new ArrayList();
		
		 
		 if(agdao.checkifagent(username)){
			 
		List a = new ArrayList();
		 a = dao.getonly(username);
		
		 Iterator propit = a.iterator();
			while(propit.hasNext()){
				 PropertyFeature prop = (PropertyFeature) propit.next();
			
					 b.add(prop);
				
			}
		
		 
		return new ModelAndView("agentviewlist","properties", b);
		 }
		 else
		 {
			 result.rejectValue("username", "cannotfindagent");
			 return new ModelAndView("agentnav");
		 }
		 
	}
	
	@RequestMapping(value="/delete.htm", method=RequestMethod.GET)
	public String deleteprop(@ModelAttribute("userprop") PropertyFeature userprop, BindingResult result, @RequestParam("name") String name) throws AdException{
		
		PropertyfDAO dao= new PropertyfDAO();
		
		PropertyFeature pf= dao.get(name);
		
		dao.delete(name);
		
		return  "success";
		
		
	}
	
	@RequestMapping(value="/edit.htm", method=RequestMethod.GET)
	public ModelAndView editform(@ModelAttribute("userprop") PropertyFeature userprop, BindingResult result, @RequestParam("name") String name){
		
		PropertyfDAO dao= new PropertyfDAO();
		
		PropertyFeature pf= dao.get(name);
		
		return new ModelAndView("update", "user", pf);
	}
	
	@RequestMapping(value="/agent.htm", params="updateprop", method= RequestMethod.POST)
	public ModelAndView update(@ModelAttribute("userprop") PropertyFeature userprop, BindingResult result){
		validator.validate(userprop, result);
		if(result.hasErrors()){
			//return "agent";
		
			return new ModelAndView("agent");
		}
		String username= userprop.getUsername();
		long rate = userprop.getRate();
		String location= userprop.getLocation();
		String name= userprop.getName();
		String category = userprop.getCategory();
		String propfeature= userprop.getPropfeature();
		String desc= userprop.getDescriptionofhouse();
		boolean laundry = userprop.isLaundry();
		boolean porch= userprop.isPorch();
		boolean parking = userprop.isParking();
		boolean basement = userprop.isBasement();
		int numofrooms= userprop.getNumofrooms();
		float area= userprop.getArea();
		int baths = userprop.getBaths();
		AgentDAO agentdao = new AgentDAO();
		PropertyfDAO property = new PropertyfDAO();
		
		try{
			
			
			Agent agent = agentdao.get(username);
			
			PropertyFeature prop = property.update(username, rate, location, name, category, propfeature, desc, laundry, porch, parking, basement, numofrooms, area, baths,agent);		
		
			
			
		} catch (AdException e) {
            System.out.println(e.getMessage());
        }
		
		return new ModelAndView("redirect:/agent.htm", "prop", userprop);	


	}	
	
	
	@RequestMapping(value="/agent.htm", params="logout", method=RequestMethod.POST)
	public String logout(@ModelAttribute("userprop") PropertyFeature userprop, BindingResult result){
	
		return "logout";
		
	}
	
	@RequestMapping(value="/agent.htm",method= RequestMethod.GET)
	public String initializeForm(@ModelAttribute("userprop") PropertyFeature userprop, BindingResult result){
		return "agent";
	}

	
	
	
}
