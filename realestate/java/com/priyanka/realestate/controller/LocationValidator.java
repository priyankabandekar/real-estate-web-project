package com.priyanka.realestate.controller;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.priyanka.realestate.pojo.PropertyFeature;

@Component
public class LocationValidator implements Validator {

	public boolean supports(Class aClass){
		return aClass.equals(PropertyFeature.class);
	}

	public void validate(Object obj, Errors errors)
	{
		PropertyFeature user = (PropertyFeature) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location", "error.invalid.user", "Invalid Details- Location Required");
	}
}
