package com.priyanka.realestate.controller;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.priyanka.realestate.pojo.PropertyFeature;
@Component
public class PropertyValidator implements Validator{
	
	public boolean supports(Class aClass){
		return aClass.equals(PropertyFeature.class);
	}
	
	
	public void validate(Object obj, Errors errors)
	{
		PropertyFeature user = (PropertyFeature) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "error.invalid.user", "Invalid Details- User Name Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rate", "error.invalid.user", "Invalid Details- Rate Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location", "error.invalid.user", "Invalid Details- Location Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "error.invalid.user", "Invalid Details- Name Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "category", "error.invalid.user", "Invalid Details- Category Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propfeature", "error.invalid.user", "Invalid Details- Property Feature Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descriptionofhouse", "error.invalid.user", "Invalid Details- Description Of House required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "laundry", "error.invalid.user", "Invalid Details- Laundry details required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "porch", "error.invalid.user", "Invalid Details- Porch details required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "parking", "error.invalid.user", "Invalid Details- Parking required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "numofrooms", "error.invalid.user", "Invalid Details- NUmber of Rooms required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "area", "error.invalid.user", "Invalid Details- Area required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "baths", "error.invalid.user", "Invalid Details- Number of Baths required");
	}
	

}
