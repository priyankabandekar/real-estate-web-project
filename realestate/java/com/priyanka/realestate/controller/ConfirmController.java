package com.priyanka.realestate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.priyanka.realestate.dao.AgentDAO;
import com.priyanka.realestate.dao.PropertyfDAO;
import com.priyanka.realestate.dao.buyerDAO;
import com.priyanka.realestate.exception.AdException;
import com.priyanka.realestate.pojo.Agent;
import com.priyanka.realestate.pojo.PropertyFeature;
import com.priyanka.realestate.pojo.buyer;

@Controller
@RequestMapping("/confirm.htm")
public class ConfirmController {
	
	
	
	@RequestMapping(method= RequestMethod.POST)
	public String doonsubmit(@ModelAttribute("buyy") PropertyFeature buyy, BindingResult result){
		
		
		if(result.hasErrors()){
			return "confirm";
			
		}
		
		String propname= buyy.getName();
		String username= buyy.getBuyerr().getUsername();
		AgentDAO adao= new AgentDAO();
		buyerDAO dao= new buyerDAO();
		PropertyfDAO pdao = new PropertyfDAO();
		
		if(pdao.checkifpropertyexixts(propname)&& dao.checkifbuyer(username) )
		{
		try{
			
			
			
			buyer buyer = dao.get(username);
			
			PropertyFeature p = pdao.get(propname);
			System.out.println("hi");
			
			p.setBuyerr(buyer);
			
			
			
			buyer.addprop(p);
			System.out.println("hi");
			dao.save(buyer);
			System.out.println("hi");
			Agent ag=p.getAgent();
			buyer bu = p.getBuyerr();
			
			bu.addAgent(ag);
			dao.save(buyer);
			
			ag.addbuyer(bu);
			adao.save(ag);
			
			
			
			
			
			
		} catch (AdException e) {
            System.out.println(e.getMessage());
        }
		
		return "bought";
		}
		else
		{
			result.rejectValue("username", "doesnotexist");
			return "buy";
		}
		
	}
	
	@RequestMapping(params="logout", method=RequestMethod.POST)
	public String logout(@ModelAttribute("userprop") PropertyFeature userprop, BindingResult result){
		return "logout";
		
	}
	

}
