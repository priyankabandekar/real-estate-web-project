<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" >
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<style>
.login{float:right;margin-right: 1%;}
.logout{float:right;margin-right: 2%;}
.register{ float:left;
}
.logins{
	margin-bottom: 15px;
}
.btn-warning,.btn-success,.btn-danger,.btn-primary,.btn-info
{
	display: inline-block;
  padding: 15px 25px;
  cursor: pointer;
  text-align: center;	
  text-decoration: none;
  
  border: none;
  border-radius: 15px;
  box-shadow: 0 9px #999;
  }
  
.btn-warning:active,.btn-success:active,.btn-danger:active,.btn-primary:active,.btn-info:active{
	box-shadow: 0 5px #666;
  transform: translateY(4px);

}
button{
	 outline:none !important;
}
</style>
</head>

<body>
<div class="container-fluid">
<div class="page-header">
<div class="row">
<div class="col-md-3">
<img src="resources/images/11.png" alt="Real Estate website" width="100%" height="200px">
</div>
<div class="col-md-9">
<img src="resources/images/1.gif" alt="Real Estate website" width="100%">
</div>
</div><!-- end of row-->
</div>


</div>

<div class="container">
<div class="row">
<form>
	 <div class= "col-md-8 logins ">
                  
				   <br><a href="register.htm"><button type="button" class="btn btn-primary register" >Register</button></a><br><br>
	
       </div> 
	   
	   <div class= "col-md-4 logins">
                  <br>
				  <a href="login.htm"><button type="button" class="btn btn-info login">Sign In</button></a>
				  
	
       </div> 
       </form>
</div>
</div>

<div class="row ">

 <div class="col-md-12">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
  <li data-target="#myCarousel" data-slide-to="1"></li>
  <li data-target="#myCarousel" data-slide-to="2"></li>
  <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>
 <!-- Wrapper for slides -->
 <div class="carousel-inner" role="listbox">
 <div class="item active">
 <img src="resources/images/a.jpg" alt="Sale item 1" width="100%" height="125px">
 </div>
<div class="item">
<img src="resources/images/b.jpg" alt="Sale Item 2" width="100%" height="25px" >
</div>
<div class="item">
<img src="resources/images/c.jpg" alt="Sale Item 3" width="100%" height="25px" >
</div>
<div class="item">
<img src="resources/images/d.jpg" alt="Sale Item 4" width="100%" height="25px" >
</div>
 </div>
<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
<span class="sr-only">Next</span>
</a>
 </div>
 
 
</div><!-- midcol-->




</div><!-- end of row-->


</div>
</body>
</html>