<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<title>List of Property </title>
<style>
.btn-warning,.btn-success,.btn-danger,.btn-primary,.btn-info
{
	display: inline-block;
  padding: 15px 25px;
  cursor: pointer;
  text-align: center;	
  text-decoration: none;
  
  border: none;
  border-radius: 15px;
  box-shadow: 0 9px #999;
  }
  
.btn-warning:active,.btn-success:active,.btn-danger:active,.btn-primary:active,.btn-info:active{
	box-shadow: 0 5px #666;
  transform: translateY(4px);

}
button{
	 outline:none !important;
}

</style>
</head>
<body>
<div class="container-fluid">
<div class="page-header">
<div class="row">
<div class="col-md-3">
<img src="resources/images/11.png" alt="Real Estate website" width="100%" height="200px">
</div>
<div class="col-md-9">
<img src="resources/images/1.gif" alt="Real Estate website" width="100%">
</div>
</div><!-- end of row-->
</div>

<div class="container">
        <table class="table table-striped table-hover table-responsive">
        <thead>
            <tr>
                <th>Property Name</th>
                <th>Rate</th>
                <th>Location</th>
                <th>Category</th>
                <th>Property Feature</th>
                <th>Description</th>
                <th>Laundry</th>
                <th>Porch</th>
                <th>Parking</th>
                <th>Basement</th>
                <th>Number of Rooms</th>
                <th>Area</th>
                <th>Bath</th>
                <th>AgentID</th>
                <th>BuyerID</th>
                

            </tr>
            </thead>
            <tbody>
            <c:forEach var="adv" items="${properties}">
                <tr>
                    <td>${adv.name}</td>
                    <td>${adv.rate}</td>
                    <td>${adv.location}</td>
                    <td>${adv.category}</td>
                    <td>${adv.propfeature}</td>
                    <td>${adv.descriptionofhouse}</td>
                    <td>${adv.laundry}</td>
                    <td>${adv.porch}</td>
                    <td>${adv.parking}</td>
                    <td>${adv.basement}</td>
                    <td>${adv.numofrooms}</td>
                    <td>${adv.area}</td>
                    <td>${adv.baths}</td>
                    <td>${adv.agent}</td>
                    <td>${adv.buyerr}</td>
                    <td><a href="edit.htm?name=${adv.name}">Edit</a> | <a href="delete.htm?name=${adv.name}">Delete</a>
                </tr>
            </c:forEach>
            </tbody>
        </table>
       
        <div class="row">
	<div class="col-sm-6 col-sm-offset-3">
	<br><a href="agent.htm"><button type="submit" name="addprop" class="btn btn-primary btn-block">Add Property</button></a><br><br>
	</div>
	</div>
	<form:form action="agent.htm" method="post">
	<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
	<br><a href="logout.htm"><button type="submit" name="logout" class="btn btn-primary btn-block" >Logout</button></a><br><br>
	</div>
	</div>
	</form:form>
	
</div>
</div>
</body>
</html>