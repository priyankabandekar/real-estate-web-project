<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" >
<script type="text/javascript" src="./js/menu.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<title>Agent Register </title>
<style>
.btn-warning,.btn-success,.btn-danger,.btn-primary,.btn-info
{
	display: inline-block;
  padding: 15px 25px;
  cursor: pointer;
  text-align: center;	
  text-decoration: none;
  
  border: none;
  border-radius: 15px;
  box-shadow: 0 9px #999;
  }
  
.btn-warning:active,.btn-success:active,.btn-danger:active,.btn-primary:active,.btn-info:active{
	box-shadow: 0 5px #666;
  transform: translateY(4px);

}
button{
	 outline:none !important;
}

</style>
</head>
<body>
<div class="container-fluid">
<div class="page-header">
<div class="row">
<div class="col-md-3">
<img src="resources/images/11.png" alt="Real Estate website" width="100%" height="200px">
</div>
<div class="col-md-9">
<img src="resources/images/1.gif" alt="Real Estate website" width="100%">
</div>
</div><!-- end of row-->
</div>

<div class="container">
<h1> AGENT </h1>

<form:form action="agent.htm" commandName="userprop" method="post">

<h2>${user.username} </h2>
<h2> ${prop.username} </h2>
<br>
<h1> Update PROPERTY</h1>


Username: <form:input path="username" value="${user.username}" size="30" class="form-control"/><font color="red"><form:errors path="username"/></font><br><br>
Rate: <form:input path="rate" size="30" value="${user.rate}" class="form-control"/><font color="red"><form:errors path="rate"/></font><br><br>
Location: <form:input path="location" size="30" value="${user.location}" class="form-control"/><font color="red"><form:errors path="location"/></font><br><br>
Name: <form:input path="name" size="30" value="${user.name}" class="form-control"/><font color="red"><form:errors path="name"/></font><br><br>
Category: <form:input path="category" size="30" value="${user.category}" class="form-control"/><font color="red"><form:errors path="category"/></font><br><br>
Property Feature: <form:input path="propfeature" size="30" value="${user.propfeature}" class="form-control"/><font color="red"><form:errors path="propfeature"/></font><br><br>
Description: <form:input path="descriptionofhouse" size="10" value="${user.descriptionofhouse}" class="form-control"/><font color="red"><form:errors path="descriptionofhouse"/></font><br><br>
Laundry: <form:input path="laundry" size="30" value="${user.laundry}" class="form-control"/><font color="red"><form:errors path="laundry"/></font><br><br>
Porch: <form:input path="porch" size="30" value="${user.porch}" class="form-control"/><font color="red"><form:errors path="porch"/></font><br><br>
Parking: <form:input path="parking" size="30" value="${user.parking}" class="form-control"/><font color="red"><form:errors path="parking"/></font><br><br>
Rooms:<form:input  path="numofrooms" size="30" value="${user.numofrooms}" class="form-control"/><font color="red"><form:errors path="numofrooms"/></font><br><br>
Area: <form:input path="area" size="30" value="${user.area}" class="form-control"/><font color="red"><form:errors path="area"/></font><br><br>
Bath: <form:input path="baths" size="20" value="${user.baths}" class="form-control"/><font color="red"> <form:errors path="baths"/></font><br><br>



<br><button type="submit" name="updateprop" class="btn btn-primary register"> Update Property </button><br><br>
<div class="row">
<div class="col-sm-6">
<br><a href="agentnav.htm"></a><button type="submit" name="nav" class="btn btn-primary register"> Next </button><br><br>
</div>
<div class="col-sm-6">
<br><a href="logout.htm"><button type="submit" name="logout" class="btn btn-primary" >Logout</button></a><br><br>
</div>
</div>
</form:form>


</div>
</div>
</body>
</html>