<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" >
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<title>Register</title>
<style>
.btn-warning,.btn-success,.btn-danger,.btn-primary,.btn-info
{
	display: inline-block;
  padding: 15px 25px;
  cursor: pointer;
  text-align: center;	
  text-decoration: none;
  
  border: none;
  border-radius: 15px;
  box-shadow: 0 9px #999;
  }
  
.btn-warning:active,.btn-success:active,.btn-danger:active,.btn-primary:active,.btn-info:active{
	box-shadow: 0 5px #666;
  transform: translateY(4px);

}
button{
	 outline:none !important;
}

</style>
</head>
<body>
<div class="container-fluid">
<div class="page-header">
<div class="row">
<div class="col-md-3">
<img src="resources/images/11.png" alt="Real Estate website" width="100%" height="200px">
</div>
<div class="col-md-9">
<img src="resources/images/1.gif" alt="Real Estate website" width="100%">
</div>
</div><!-- end of row-->
</div>

<div class="container">
<h1> REGISTRATION</h1>
<form:form action="register.htm" commandName="user" method="post">

First Name: <form:input path="fname" size="30"/><font color="red"><form:errors path="fname"/></font><br><br>
Last Name: <form:input path="lname" size="30"/><font color="red"><form:errors path="lname"/></font><br><br>
Street: <form:input path="address.street" size="30"/><font color="red"><form:errors path="address.street"/></font><br><br>
City: <form:input path="address.city" size="30"/><font color="red"><form:errors path="address.city"/></font><br><br>
State: <form:input path="address.state" size="30"/><font color="red"><form:errors path="address.state"/></font><br><br>
Pincode: <form:input path="address.pincode" size="10"/><font color="red"><form:errors path="address.pincode"/></font>
Date of Birth: <form:input type="date" value="1993-03-12" path="dob" size="30"/><font color="red"><form:errors path="dob"/></font><br><br>
SSN: <form:input path="ssn" size="30"/><font color="red"><form:errors path="ssn"/></font><br><br>
User Name: <form:input path="username" size="30"/><font color="red"><form:errors path="username"/></font><br><br>
Password:<form:input type="password" path="password" size="30"/><font color="red"><form:errors path="password"/></font><br><br>
Email: <form:input type="email" path="email" size="30"/><font color="red"><form:errors path="email"/></font><br><br>
Type: <form:input path="type" size="20"/><font color="red"> <form:errors path="type"/></font><br><br>

 <br><button type="submit" class="btn btn-primary" value="Register User">Register</button>
 <a href="home.jsp"><button type="button" class="btn btn-primary" >BACK HOME</button></a><br><br>
 
 </form:form>
</div>
</div>
</body>
</html>